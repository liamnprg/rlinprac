# RLinprac

Contrary to its poor name, `rlinprac` is a linear algebra calculator capable of taking matrices as input and performing common classroom operations. 

## Dependencies

RLinprac depends on lapack if the `lapack_integration` feature is enabled. The integration with lapack provides the `eigenvectors` function.

To build with lapack, one must have a C compiler that recognizes the `-llapack` library flag. This corresponds to the FORTRAN lapack interface, not lapacke, the C interface. 

### Note on linking difficulties

I have had linking troubles with lapack on a couple different systems. If your compiler/linker cannot find `ld -llapack`, then the lapack integration will not work. 

OpenBLAS, which provides blas and lapack, seems to be the trickiest, although I have gotten it to work on Gentoo (only after also installing the reference implementation, which might have pulled in `virtual/lapack`. Consult the docs for your linux distro of choice.

Protip: Use `ldconfig -p | grep lapack` to test if your system has a lapack installation.

#### Final linking note

Blas is not needed for this project, only lapack.

## Cross-compilation?

I cross compiled this for windows, but turned the `lapack_integration` feature off. I used `cargo rustc --release --target x86_64-pc-windows-gnu --no-default-features` after installing the target using rustup alongside a mingw toolchain compiled via the lovely crossdev tool.

## Help

```
List of unary functions:
	svdvt
	eigenvectors
	tr
	t
	qrq
	i
	projmat
	show
	dim
	basis
	eigenvalues
	qrr
	inv
	svdu
	svdsv
	gramschmidt
	sqrt
	null
	det
List of binary functions:
	sub
	proj
	leastsquare
	solve
	union
	add
	div
	mul
How to enter matrices:

    >A = [3x2 -1 2.5 3 4 5 6+i]
    Added variable named A with matrix 
  ┌               ┐
  │  -1+0i 2.5+0i │
  │   3+0i   4+0i │
  │   5+0i   6+1i │
  └               ┘

How to call functions:

    >B = inv A
    >union A B
```            


## Shortcomings

* Only capable of parsing simple expressions with one assignment, one function, and one or two arguments. 
* The parser is very poor, written as a series of string splits and if statements. It does seem pretty reliable as I deem that a well-written parser would not be worth the overhead of a strong token and expression system. 
* No help statements explaining what each function does
* Inefficient, poor implementations were chosen, and memory footprint was not a concern when writing. This was because I could not care less to save the bytes worth of storage that were being used for each `.clone()` operation.


## Commits welcome!

If you want to add more functions, or a better expressions parser, that would be appreciated. I primarily wrote this project because no calculators existed for the operations I was concerned with for my MATB24 course. I cannot guarantee quick response times though.

## A series of complaints and complements

* the Rust lapack bindings ecosystem is really, really confusing and poorly written. The lapack bindings do not seem to link executables to the lapack system library, which resulted in a very clumsy `extern "C"` declaration in main.rs. It seems like this has something to do with rust growing pains, but I'm not sure.
* Nalgebra is a really great library which is really useful, it was a pleasure to use, even for somebody who did not know very much about matrix decompositions and had to do a lot of Wikipedia-guess-and-check. Favorite feature: Pretty-printing matrices.
* Lapack is a horrible library that I might use in the future because it's also really fast and awesome. I find the prospect of linking Rust and Fortran code together very funny.
* Please no more "\*-src" packages. It really takes a toll on compilation times for the default option to be "compile lapack each time for every person". Especially when people do not know that this is happening, and might not want to compile large libraries. 
* It's really hard to tell downstream packages how to behave. I did not want, for example, to compile lapack each time on all 3 systems I was using to write this library, but it was very difficult (or impossible) to tell nalgebra-lapack to tell lapack to tell lapack-sys to NOT compile lapack/netlib. 
* Some of these might be due to a lack of knowledge about Rust conditional-compilation and associated topics. In my defense, there is little info online about how to do things "properly" or if that is even possible.
* Rust only has second-class support for Complex numbers, and, it would seem, no native support for epsilon comparisons. This results in clumsy code.
