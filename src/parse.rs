use nalgebra::*;
use std::collections::HashMap;
use std::str::FromStr;
use std::error::Error;
use std::boxed::Box;
use std::process::exit;
use num_complex::Complex;
use simple_error::*;
use crate::funcs::*;

//10^5=100,000, 5 digits
pub const EPSILON: f64 = 100_000.0;

fn get_line() -> String {
        use std::io::stdin;
        let mut s: String = String::new();
        let res = stdin().read_line(&mut s);
        match res {
            Ok(0) => exit(0),
            Ok(_) => s.pop(),
            Err(_) => exit(1),
        };
        return s;
}

fn print_eps_mat(m: &DMatrix<Complex<f64>>) {
    let nm = m.map(|e| (e*EPSILON).round()/EPSILON);
    println!("{}",nm);
}

/*
> A = [3x3 1 1 1 0 1 0 0 1 1]
> B = eigenvalues A
> eigenvalues A
> eigenbasis A
> C = gramschmidt A
> v = [3x1 1 1 1]
> is_basis A
> union A B
true
> coord = coordinates A v
> show coord
> r1 = row A 1
> c1 = column A 1
*/


pub struct Cli {
    vars: HashMap<String,DMatrix<Complex<f64>>>,
    fns: HashMap<String,Box<dyn UniFn>>,
    bfns: HashMap<String,Box<dyn BiFn>>
}

impl Cli {
    pub fn new() -> Cli {
        Cli {
            vars: HashMap::new(),
            fns: HashMap::new(),
            bfns: HashMap::new()
        }
    }
    pub fn register_fn<T>(&mut self, name: &str, func: T)
        where T: 'static + UniFn {

            self.fns.insert(name.to_string(),Box::new(func));
    }
    pub fn register_bfn<T>(&mut self, name: &str, func: T)
        where T: 'static + BiFn {
            self.bfns.insert(name.to_string(),Box::new(func));
    }
    pub fn get_var(&self,name: &str) -> Option<DMatrix<Complex<f64>>> {
        let maybenum = Complex::<f64>::from_str(&name);
        if let Ok(number) = maybenum {
            return Some(dmatrix!(number));
        }
        Some(self.vars.get(name)?.clone())
    }
    pub fn parse_eq(&mut self) -> Result<(),Box<dyn Error>> {
        let s  = get_line();
        if s.trim() == "help" {
            println!("List of unary functions:");
            for f in self.fns.keys() {
                println!("\t{}",f);
            }
            println!("List of binary functions:");
            for f in self.bfns.keys() {
                println!("\t{}",f);
            }
            println!("How to enter matrices:");
            println!("
    >A = [3x2 -1 2.5 3 4 5 6+i]
    Added variable named A with matrix 
  ┌               ┐
  │  -1+0i 2.5+0i │
  │   3+0i   4+0i │
  │   5+0i   6+1i │
  └               ┘
");


            println!("How to call functions:");
            println!("
    >B = inv A
    >union A B
            ");

            return Ok(());
        } else if s.trim() == "" {
            return Ok(());
        }
        let v: Vec<&str> = s.split('=').collect();
        let mut assign_to_var = false;
        if v.len() == 2 {
            assign_to_var = true;
        }
        let mut binary = false;
        let v_split_on_space: Vec<&str>;
        //entering new vector or matrix
        if assign_to_var {
            v_split_on_space = v[1].split(' ').filter(|x| x != &"").collect();
        } else {
            v_split_on_space = v[0].split(' ').filter(|x| x != &"").collect();
        }
        let index = {
            if assign_to_var {
                1
            } else {
                0
            }
        };
        if v_split_on_space.len() == 2 {
            binary = false;
        } else if v_split_on_space.len() == 3 && !v[index].contains('[') {
            binary = true;
        }
        if binary {
            let newmat: DMatrix<Complex<f64>>;
            let varname = v[0].trim().to_string();
            let funcargs: Vec<&str>; 
            if assign_to_var {
                funcargs = v[1].split(' ').filter(|x| x != &"").collect();
            } else {
                funcargs = v[0].split(' ').filter(|x| x != &"").collect();
            }
            let mut func = funcargs[0].trim().to_string();
            func.make_ascii_lowercase();
            let rustfn = self.bfns.get(&func).ok_or(format!("Could not find function {}", func))?;

            let arg1 = funcargs[1].trim().to_string();
            let arg2 = funcargs[2].trim().to_string();
            let mat1 = self.get_var(&arg1).ok_or(format!("Could not find variable {}", arg1))?;

            let mat2 = self.get_var(&arg2).ok_or(format!("Could not find variable {}", arg2))?;

            newmat = rustfn(&mat1,&mat2)?;
            if assign_to_var {
                println!("Added variable named {} with matrix {}", &varname,&newmat);
                self.vars.insert(varname,newmat);
            } else {
                print_eps_mat(&newmat);
            }
            return Ok(());
        }

        if assign_to_var && !binary {
            if v[1].contains('[') {
                let varname = v[0].trim().to_string();
                let dmat = parse_mat(v[1].trim().to_string())?;
                println!("Added variable named {} with matrix {}", &varname,&dmat);
                self.vars.insert(varname,dmat);
                return Ok(());
            } else {
                let varname = v[0].trim().to_string();
                let funcargs: Vec<&str> = v[1].split(' ').filter(|x| x != &"").collect();
                let mut func = funcargs[0].trim().to_string();
                func.make_ascii_lowercase();
                let rustfn = self.fns.get(&func).ok_or(format!("Could not find function {}", func))?;


                let args = funcargs[1].trim().to_string();
                let mat = self.get_var(&args).ok_or(format!("Could not find variable {}", args))?;
                let newmat = rustfn(&mat)?;
                println!("Added variable named {} with matrix {}", &varname,&newmat);
                self.vars.insert(varname,newmat);
                return Ok(());
            }
        } else if !binary {
            let funcargs: Vec<&str> = v[0].split(' ').filter(|x| x != &"").collect();
            let mut func = funcargs[0].trim().to_string();
            func.make_ascii_lowercase();
            let rustfn = self.fns.get(&func).ok_or(format!("Could not find function {}", func))?;



            let args = funcargs[1].trim().to_string();
            let mat = self.get_var(&args).ok_or(format!("Could not find variable {}", args))?;
            let newmat = rustfn(&mat)?;
            print_eps_mat(&newmat);
            return Ok(());
        }
        println!("operation not understood");
        Ok(())
    }
}

fn parse_mat(s: String) -> Result<DMatrix<Complex<f64>>,Box<dyn Error>> {
    let mut s = s.clone();
    s = s.chars().filter(|x| !(*x == '[' || *x == ']')).collect();

    let mut rowcolmat: Vec<&str> = s.split(|x: char| x == 'x' || x == ' ')
        .filter(|x| x != &"")
        .collect();
    let rows = usize::from_str(rowcolmat.remove(0))?;
    let cols = usize::from_str(rowcolmat.remove(0))?;

    let mut matf = vec!();
    for sf in rowcolmat {
        let cf = Complex::<f64>::from_str(&sf)?;
        matf.push(cf);
    }
    if matf.len() != rows*cols {
        return Err(Box::new(simple_error!("Not enough elements to fill an {}x{} element matrix", rows,cols)));
    }
    return Ok(DMatrix::from_row_slice(rows,cols,&matf));
}
