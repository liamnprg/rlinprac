use nalgebra::*;
#[cfg(feature = "lapack_integration")]
use lapack::*;
use simple_error::*;
use std::error::Error;
use crate::parse::EPSILON;
use trait_set::trait_set;

const ZERO: Complex<f64> = Complex::new(0.0,0.0);

pub type ResMat = Result<DMatrix<Complex<f64>>,Box<dyn Error>>;
macro_rules! UniFn {
    () => { Fn(&DMatrix<Complex<f64>>) -> ResMat };
}
trait_set! {
pub trait UniFn = Fn(&DMatrix<Complex<f64>>) -> ResMat;
pub trait BiFn = Fn(&DMatrix<Complex<f64>>,&DMatrix<Complex<f64>>) -> ResMat;
}


//orthogonalize an eigenbasis with column-vectors m using the QR algorithm (Q is orthogonal)
pub fn gramschmidt(m: &DMatrix<Complex<f64>>) -> ResMat {
    let ortho = m.clone();
    Ok(ortho.qr().q())
}

pub fn show(m: &DMatrix<Complex<f64>>) -> ResMat {
    Ok(m.clone())
}

pub fn jordan(m: &DMatrix<Complex<f64>>) -> ResMat {
    let (row,col) = m.shape();
    if row != col {
        bail!("input matrix must be square");
    }
    let evals = eigenvalues(m)?.data.as_vec().clone();
    let dimnull = |m| {
        dim(&null(&m).unwrap()).unwrap()[0].re.abs().floor() as usize
    };
    let last = |v: &Vec<usize>| {
        v.last().unwrap().clone()
    };
    let mut dim_count_eigenvalues = vec![];
    for eval in evals {
        println!("For eigenvalue {}",eval);
        let mut dimnulls = vec!();
        let mut i = 1;
        let charmat = m-crate::funcs::i(&dmatrix!(Complex::new(row as f64,0.0)))?.scale(eval.re);
        let mut dimnull_i = dimnull(charmat.pow(i).unwrap());
        dimnulls.push(dimnull_i);
        println!("dim null({}^{})={}",charmat.pow(i).unwrap(),i,dimnull_i);
        i+=1;
        dimnull_i = dimnull(charmat.pow(i).unwrap());
        dimnulls.push(dimnull_i);
        println!("dim null({}^{})={}",charmat.pow(i).unwrap(),i,dimnull_i);
        while last(&dimnulls) != dimnull_i {
            //does rust optimize this?
            dimnull_i = dimnull(charmat.pow(i).unwrap());
            dimnulls.push(dimnull_i);
            println!("dim null({}^{})={}",charmat.pow(i).unwrap(),i,dimnull_i);
            i+=1;
        }
        let mut dimnulls_prev = [vec![0],vec![0],dimnulls.clone()].concat();

        let mut dimnulls_next = [dimnulls.clone(),vec![0],vec![0]].concat();
        let mut dimnulls = [vec![0],dimnulls,vec![0]].concat();
        let mut prev_cur_next: Vec<((usize,usize),usize)> = dimnulls_prev.into_iter().zip(dimnulls.clone().into_iter()).zip(dimnulls_next.into_iter()).collect();
        println!("{:?}",&prev_cur_next);
        prev_cur_next.remove(0);
        prev_cur_next.pop();
        prev_cur_next.pop();
        println!("{:?}",&prev_cur_next);
        let dim_count: Vec<(usize,usize)> = prev_cur_next.into_iter().zip(1..).map(|(((prev,cur),next),i)| (2*cur-prev-next,i)).collect();
        println!("{:?}",dim_count);
        let dim_count_eigenvalue: Vec<(usize,usize,Complex<f64>)> = dim_count.into_iter().map(|(dim,count)| (dim,count,eval)).collect();
        for dce in dim_count_eigenvalue {
            dim_count_eigenvalues.push(dce);
        }
    }
    println!("(block dimension,count): {:?}",dim_count_eigenvalues);
    let mut jf = dmatrix!();
    jf.resize_mut(row,col,ZERO);
    let mut i = 0;
    let mut dim;
    let mut count;
    for (dim_b,count_b,eval) in dim_count_eigenvalues {
        dim=dim_b;
        count=count_b;
        while count != 0 {
            if dim == 1 {
                jf[(i,i)] = eval;
                i+=1;
            } else {
                while dim != 1 {
                    jf[(i,i)] = eval;
                    jf[(i,i-1)] = Complex::new(1.0,0.0);
                    dim-=1;
                    i+=1;
                }
            }
            dim=dim_b;
            count-=1;
        }
    }
    return Ok(jf);
}

pub fn i(m: &DMatrix<Complex<f64>>) -> ResMat { 
    let (r,c) = m.shape();
    if r != c {
        bail!("Input matrix must be square");
    }
    let n = m[0].re.floor() as usize;
    Ok(DMatrix::identity(n,n))
}

pub fn sqrt(m: &DMatrix<Complex<f64>>) -> ResMat {
    Ok(m.map(|x| x.sqrt()))
}

pub fn eigenvalues(m: &DMatrix<Complex<f64>>) -> ResMat {
    //need better implementation
    //use lapack if lapack_integration is on
    let evals = m.eigenvalues().unwrap();
    Ok(DMatrix::from_column_slice(m.shape().0,1,evals.data.as_vec()))
}      

// calculate the null space of matrix m and find a basis using the U vector in the SVD of m
pub fn null(m: &DMatrix<Complex<f64>>) -> ResMat {
    if m.is_empty() {
        let (arow,acol) = m.shape();
        let dim = min(arow,acol);
        return i(&dmatrix!(Complex::new(dim as f64,0.0)));
    }
    let rank = m.rank(0.1);
    let t = t(m)?;
    let mat_with_extra = svdu(&t)?;
    let mat = mat_with_extra.remove_columns(0,rank);
    return Ok(mat);
}


//same as gramschmidt
pub fn qrq(m: &DMatrix<Complex<f64>>) -> ResMat {
    let m = m.clone();
    Ok(m.qr().q())
}

//returns the R matrix in QR decomposition
pub fn qrr(m: &DMatrix<Complex<f64>>) -> ResMat {
    let m = m.clone();
    Ok(m.qr().r())
}

//matrix transpose
pub fn t(m: &DMatrix<Complex<f64>>) -> ResMat {
    Ok(m.adjoint())
}

//U matrix in SVD
pub fn svdu(m: &DMatrix<Complex<f64>>) -> ResMat {
    Ok(m.clone().svd(true,false).u.ok_or(simple_error!("No SVD found"))?)
}

//V^T matrix in SVD
pub fn svdvt(m: &DMatrix<Complex<f64>>) -> ResMat {
    Ok(m.clone().svd(false,true).v_t.ok_or(simple_error!("No SVD found"))?)
}

//\sum matrix in SVD, aka the single values of m
pub fn svdsv(m: &DMatrix<Complex<f64>>) -> ResMat {
    let sv = m.clone().svd(false,false).singular_values;
    let svdata = sv.data.as_vec().clone();
    let svprop: Vec<Complex<f64>> = svdata.into_iter().map(|x| Complex::new(x,0.0)).collect();


    Ok(DMatrix::from_column_slice(m.shape().0,1,&svprop))
}

#[cfg(feature = "lapack_integration")]
//calculate all real eigenvectors for matrix m, complex eigenvectors are not calculated
pub fn eigenvectors(m: &DMatrix<Complex<f64>>) -> ResMat {
    let mut a = m.data.as_vec().clone();
    let (rows,col) = m.shape();
    assert_eq!(rows,col);

    //n is rows, not rows*col
    let n = rows as i32;
    let lda = n;
    
    //contains the computed eigenvalues
    let mut w = vec![Complex::new(0.0,0.0); n as usize];
        
    //contains left eigenvectors
    let mut vl = vec![Complex::new(0.0,0.0); rows*col];

    //ldvl is an int, the leading dimension of the array VL. LDVL >=1; if JOBVL='V', LDVL >=n.
    //this value is probably fine
    let ldvl = 1;

    //right eigenvectors
    let mut vr = vec![Complex::new(0.0,0.0); rows*col];

    let ldvr = n;

    let lwork = 4*n;
    //where computations are performed
    let mut work = vec![Complex::new(0.0,0.0); lwork as usize];
    let mut info = 0;

    let twon = 2*n as usize;
    let mut rwork = vec![0.0; twon];

    unsafe {
        zgeev('N' as u8,'V' as u8,n,&mut a,lda,&mut w,&mut vl,ldvl,&mut vr,ldvr,&mut work,lwork,&mut rwork, &mut info);
    }
    Ok(DMatrix::from_vec(rows,col,vr))
}

//matrix trace
pub fn tr(m: &DMatrix<Complex<f64>>) -> ResMat {
    Ok(dmatrix!(m.trace()))
}

//matrix determinant
pub fn det(m: &DMatrix<Complex<f64>>) -> ResMat {
    Ok(dmatrix!(m.determinant()))
}

//matrix inverse
pub fn inv(m: &DMatrix<Complex<f64>>) -> ResMat {
    let (row,col) = m.shape();
    if row != col {
        bail!("Can't invert a non-square matrix");
    }
    Ok(m.clone().try_inverse().ok_or(simple_error!("no inverse"))?)
}

//calculate the projection matrix for matrix A
pub fn projmat(a: &DMatrix<Complex<f64>>) -> ResMat {
    //A^TA
    let r1 = t(a)?*a;
    //(A^TA)^{-1}
    let r2 = inv(&r1)?;
    //A*(A^TA)^{-1}
    let r3 = a*&r2;
    //A*(A^TA)^{-1}*A^T
    let r4 = &r3*t(a)?;
    return Ok(r4)

}

pub fn dim(a: &DMatrix<Complex<f64>>) -> ResMat {
    let null = null(a)?;
    let (arow,acol) = a.shape();
    let ind_dim = min(arow,acol);
    let (_,null_space_dim) = null.shape();
    return Ok(dmatrix!(Complex::new((ind_dim-null_space_dim) as f64,0.0)));
}

pub fn proj(a: &DMatrix<Complex<f64>>,b: &DMatrix<Complex<f64>>) -> ResMat {
    let (_brow,bcol) = b.shape();
    if bcol != 1 {
        bail!("must input a matrix then a vector");
    }
    let pm = projmat(&basis(a)?)?;
    return mul(&pm,b);
}

fn mcp(a: DMatrixSlice<Complex<f64>>) -> DMatrix<Complex<f64>> {
    DMatrix::from_column_slice(a.shape().0,a.shape().1,a.data.clone_owned().as_vec())
}

pub fn basis(a: &DMatrix<Complex<f64>>) -> ResMat {
    let (arow,acol) = a.shape();
    if acol > arow {
        let ac1 = mcp(a.columns(0,arow));
        let ac2 = mcp(a.columns(arow,acol-arow));
        let bc1 = basis_lte_square(&ac1)?;
        let (bc1row,bc1col) = bc1.shape();
        //checking that we haven't found a complete basis for \mathbb C^n yet
        if bc1row == bc1col {
            return Ok(bc1);
        } else {
            return basis(&union(&bc1,&basis(&ac2)?)?);
        }
    } else {
        return basis_lte_square(a);
    }
}

pub fn basis_lte_square(a: &DMatrix<Complex<f64>>) -> ResMat {
    let mut nm = a.clone();
    //upper triangular
    let t = a.clone().qr().r();
    let (trow,tcol) = t.shape();
    let end = min(trow,tcol);
    let mut i = 0;
    let mut removed_column_count = 0;
    while i < end {
        let diagentry = t[(i,i)];
        if diagentry == ZERO { 
            nm=nm.remove_column(i-removed_column_count);
            removed_column_count+=1;
        }
        i+=1;
    }
    return Ok(nm);


}

//[a|b]
pub fn union(a: &DMatrix<Complex<f64>>,b: &DMatrix<Complex<f64>>) -> ResMat {
    let a = a.clone();
    let b = b.clone();
    let (arow,acol) = a.shape();
    let (brow,bcol) = b.shape();
    if arow != brow {
        bail!("Dimension mismatch, operation not possible");
    }
    let a = a.insert_columns(acol,bcol,ZERO);
    let b = b.insert_columns(0,acol,ZERO);
    Ok(a+b)
}

pub fn add(a: &DMatrix<Complex<f64>>,b: &DMatrix<Complex<f64>>) -> ResMat {
    Ok(a+b)
}

pub fn sub(a: &DMatrix<Complex<f64>>,b: &DMatrix<Complex<f64>>) -> ResMat {
    Ok(a-b)
}

pub fn mul(a: &DMatrix<Complex<f64>>,b: &DMatrix<Complex<f64>>) -> ResMat {
    let (arow,acol) = a.shape();
    let (brow,bcol) = b.shape();
    if arow == acol && acol == 1 {
        //nalgebra issue here
        let scalar = a[0].re;
        return Ok(b.scale(scalar));
    } 
    if brow == bcol && bcol == 1 {
        let scalar = b[0].re;
        return Ok(a.scale(scalar));
    } 
    Ok(a*b)
}

pub fn div(a: &DMatrix<Complex<f64>>,b: &DMatrix<Complex<f64>>) -> ResMat {
    let (brow,bcol) = b.shape();
    if brow == bcol && bcol == 1 {
        let scalar = b[0].re;
        return Ok(a.scale(1.0/scalar));
    } 
    return Err(Box::new(simple_error!("Cannot divide two matrices")));
}

//solve [a|b] through LU decomposition, b is an n-dimensional column vector with the same number of
//rows as a
pub fn solve(a: &DMatrix<Complex<f64>>, b: &DMatrix<Complex<f64>>) -> ResMat {
    let (brow,bcol) = b.shape();
    let (arow,_) = a.shape();
    if bcol != 1 || brow != arow {
        bail!("Incorrect dimensions");
    }
    return Ok(a.clone().lu().solve(b).ok_or("no solution found")?);
    
}

//calculate the least-squares solution for matrix a and column vector b
pub fn leastsquare(a: &DMatrix<Complex<f64>>, b: &DMatrix<Complex<f64>>) -> ResMat {
    let r1 = t(a)?*a;
    let r2 = inv(&r1)?;
    let r3 = r2*t(a)?;
    let r = r3*b;
    return Ok(r);
}
