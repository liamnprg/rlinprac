use crate::funcs::*;
use crate::parse::Cli;

//the rust lapack library does not actually link to lapack, the following snippet adds the -llapack
//flag to rust's chosen c compiler
#[cfg(feature = "lapack_integration")]
#[link(name="lapack")]
extern "C" {}


fn main() {
    let mut cli = Cli::new();
    //register function
    macro_rules! rfn {    
        ($x:expr) => {        
            cli.register_fn(stringify!($x),$x)        
        }        
    }     
    //register binary function
    macro_rules! rfnb {    
        ($x:expr) => {        
            cli.register_bfn(stringify!($x),$x)        
        }        
    }     
    rfn!(gramschmidt);
    rfn!(show);
    rfn!(jordan);
    rfn!(i);
    rfn!(sqrt);
    rfn!(eigenvalues);
    #[cfg(feature = "lapack_integration")]
    rfn!(eigenvectors);
    rfn!(null);
    rfn!(qrq);
    rfn!(qrr);
    rfn!(t);
    rfn!(dim);
    rfn!(basis);
    rfn!(svdu);
    rfn!(svdvt);
    rfn!(svdsv);
    rfn!(tr);
    rfn!(det);
    rfn!(inv);
    rfn!(projmat);
    rfnb!(leastsquare);
    rfnb!(div);
    rfnb!(union);
    rfnb!(proj);
    rfnb!(add);
    rfnb!(sub);
    rfnb!(mul);
    rfnb!(solve);
    loop {
        print!(">");
        use std::io::Write;
        //used because get_line in parse.rs behaves strangely with printed characters with no
        //newline afterwards
        std::io::stdout().flush().unwrap();

        //parse_eq  is poorly named, but I haven't many better names for it
        //parse_eq_and_exec?
        //eval_eq?
        //eval_inp?
        //These all rely on the notion of execution, when that is not the case
        let res = cli.parse_eq();
        match res {
            Ok(_) => {},
            Err(m) => {
                println!("{}",m);
            }
        }
    }
}
mod funcs;
mod parse;
